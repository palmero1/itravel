#!/bin/bash

# Make sure everything is up to date
sudo apt-get update
sudo apt-get upgrade

# Get and install version 8 of Node JS
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install the application
npm install

# Run the application
npm start
