import React, { Component, Fragment } from 'react';
import { LinkContainer } from "react-router-bootstrap";
import { Navbar, NavItem, Nav } from 'react-bootstrap';
import { Link, withRouter } from 'react-router-dom';
import Routes from './Routes';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      employeeId: ''
    };
  }

  componentDidMount () {
    // this componentDidMount should have the authentication service linked - Office365 or AWS Accounts
  }

  userHasAuthenticated = (authenticated, employeeId) => {
    this.setState({
      isAuthenticated: authenticated,
      employeeId: employeeId
    });
  }

  handleLogout = event => {
    this.userHasAuthenticated(false, '');
    this.props.history.push("/login");
  }

  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      employeeId: this.state.employeeId,
      userHasAuthenticated: this.userHasAuthenticated
    };

    return (
      <div className="App">
        <div>
          <Navbar inverse collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <Link to="/">iTravel</Link>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              {this.state.isAuthenticated &&
                <Nav>
                  <LinkContainer to="/coachbooking">
                    <NavItem>Coach</NavItem>
                  </LinkContainer>
                  <LinkContainer to="/trainbooking">
                    <NavItem>Train</NavItem>
                  </LinkContainer>
                  <LinkContainer to="/hotelbooking">
                    <NavItem>Hotel</NavItem>
                  </LinkContainer>
                  <LinkContainer to="/recordtrip">
                    <NavItem>Trip</NavItem>
                  </LinkContainer>
                </Nav>
              }
              <Nav pullRight>
                {this.state.isAuthenticated
                  ? <NavItem onClick={this.handleLogout}>
                      Logout
                    </NavItem>
                  : <Fragment>
                        <NavItem href="/login">
                          Login
                        </NavItem>
                    </Fragment>
                }
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
        <Routes childProps={childProps} />
      	<footer />
      </div>
    );
  }
}

export default withRouter(App);
