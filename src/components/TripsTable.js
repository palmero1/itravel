import React, { Component } from "react";
import { Alert } from 'react-bootstrap';
import ReactTable from "react-table";
import Loader from 'react-loader-spinner';

import 'react-table/react-table.css'

export default class TripsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trips: [],
      isTripsDataLoaded: true,
      tripsDataExists: false,
      isTripsDataErrored: false
    };

    this.createTripsList = this.createTripsList.bind(this);
  }

  componentDidMount() {
    this.createTripsList();
  }

  createTripsList() {
    this.setState({isTripsDataLoaded: false});

    fetch("https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/trips?employeeId=" + this.props.employeeId)
    .then(res => res.json())
    .then((json) => {
      var initialTrips = json;

      if (json.Count > 0) {
        var tempTrips = [];

        for (var i in initialTrips.Items) {
          tempTrips[i] = {"tripLocation": initialTrips.Items[i].tripLocation.S,
                          "tripStartDate": initialTrips.Items[i].tripStartDate.S,
                          "tripEndDate": initialTrips.Items[i].tripEndDate.S};
        }

        this.setState({
          isTripsDataLoaded: true,
          tripsDataExists: true,
          trips: tempTrips
        });

        return tempTrips;

      } else {
        this.setState({
          isTripsDataLoaded: true,
          tripsDataExists: false,
          isTripsDataErrored: false
        });
      }
    },
    (error) => {
      this.setState({
        isTripsDataLoaded: true,
        isTripsDataErrored: true,
        error
      });
    })
  }

  render() {
    const columns = [{
      Header: props => <span className="header">Trip Location</span>,
      accessor: 'tripLocation'
    }, {
      Header: props => <span className="header">Trip Start Date</span>,
      accessor: 'tripStartDate'
    }, {
      Header: props => <span className="header">Trip End Date</span>,
      accessor: 'tripEndDate'
    }]

    return (
      <div>
        <h3>Your Submitted Trips</h3>
        {this.state.tripsDataExists &&
          <ReactTable
            data={this.state.trips}
            columns={columns}
            minRows={0}
            defaultPageSize={5}
          />
        }
        {!this.state.tripsDataExists && this.state.isTripsDataLoaded && !this.state.isTripsDataErrored &&
          <p>You currently have no trips submitted</p>
        }
        {!this.state.isTripsDataLoaded &&
          <Loader type="TailSpin" color="light-grey" height={80} width={80} />
        }
        {this.state.isTripsDataErrored && this.state.isTripsDataLoaded &&
          <ErrorAlert />
        }
      </div>
    );
  }
}

const ErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving trip details</b>
    <p>Please refresh the page</p>
  </Alert>
)
