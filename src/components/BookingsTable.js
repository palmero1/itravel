import React, { Component } from "react";
import { Alert } from 'react-bootstrap';
import ReactTable from "react-table";
import Loader from 'react-loader-spinner';

import 'react-table/react-table.css'

export default class BookingsTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bookings: [],
      isBookingsDataLoaded: true,
      bookingsDataExists: false,
      isBookingsDataErrored: false
    };

    this.createBookingsList = this.createBookingsList.bind(this);
  }

  componentDidMount() {
    this.createBookingsList();
  }

  createBookingsList() {
    this.setState({isBookingsDataLoaded: false});

    fetch("https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/bookings?employeeId=" + this.props.employeeId)
    .then(res => res.json())
    .then((json) => {
      var initialBookings = json;

      if (json.Count > 0) {
        var tempBookings = [];

        for (var i in initialBookings.Items) {
          tempBookings[i] = {"bookingType": initialBookings.Items[i].bookingType.S.charAt(0).toUpperCase() + initialBookings.Items[i].bookingType.S.slice(1),
                             "bookingDetails": initialBookings.Items[i].bookingDetails.S};
        }

        this.setState({
          isBookingsDataLoaded: true,
          bookingsDataExists: true,
          bookings: tempBookings
        });

        return tempBookings;

      } else {
        this.setState({
          isBookingsDataLoaded: true,
          bookingsDataExists: false,
          isBookingsDataErrored: false
        });
      }
    },
    (error) => {
      this.setState({
        isBookingsDataLoaded: true,
        isBookingsDataErrored: true,
        error
      });
    })
  }

  render() {
    const columns = [{
      Header: props => <span className="header">Booking Type</span>,
      accessor: 'bookingType',
      maxWidth: 200
    }, {
      Header: props => <span className="header">Booking Details</span>,
      accessor: 'bookingDetails'
    }]

    return (
      <div>
        <h3>Your Submitted Bookings</h3>
        {this.state.bookingsDataExists &&
          <ReactTable
            data={this.state.bookings}
            columns={columns}
            minRows={0}
            defaultPageSize={5}
          />
        }
        {!this.state.bookingsDataExists && this.state.isBookingsDataLoaded && !this.state.isBookingsDataErrored &&
          <p>You currently have no bookings submitted</p>
        }
        {!this.state.isBookingsDataLoaded &&
          <Loader type="TailSpin" color="light-grey" height={80} width={80} />
        }
        {this.state.isBookingsDataErrored && this.state.isBookingsDataLoaded &&
          <ErrorAlert />
        }
      </div>
    );
  }
}

const ErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving booking details</b>
    <p>Please refresh the page</p>
  </Alert>
)
