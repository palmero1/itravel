import React from "react";
import { Route, Switch } from "react-router-dom";
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import UnauthenticatedRoute from "./components/UnauthenticatedRoute";

import Home from "./containers/Home";
import Login from "./containers/Login";
import CoachBooking from "./containers/CoachBooking";
import TrainBooking from "./containers/TrainBooking";
import HotelBooking from "./containers/HotelBooking";
import RecordTravel from "./containers/RecordTravel";
import NotFound from "./containers/NotFound";

export default ({ childProps }) =>
  <Switch>
    <AuthenticatedRoute path="/" exact component={Home} props={childProps} />
    <UnauthenticatedRoute path="/login" exact component={Login} props={childProps} />
    <AuthenticatedRoute path="/coachbooking" exact component={CoachBooking} props={childProps} />
    <AuthenticatedRoute path="/trainbooking" exact component={TrainBooking} props={childProps} />
    <AuthenticatedRoute path="/hotelbooking" exact component={HotelBooking} props={childProps} />
    <AuthenticatedRoute path="/recordtrip" exact component={RecordTravel} props={childProps} />
    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>;
