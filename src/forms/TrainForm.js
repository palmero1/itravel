import React, { Component } from 'react';
import { HelpBlock, Radio, FormGroup, FormControl, Button, ControlLabel, Alert } from 'react-bootstrap';
import Select from 'react-select';
import Loader from 'react-loader-spinner';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';
import './TrainForm.css';

var initialStations, initialTrains;

class TrainForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      alertIsHidden: true,
      error: null,
      isOriginStationLoaded: true,
      isOriginStationErrored: false,
      isDestinationStationLoaded: true,
      isDestinationStationErrored: false,
      isOutboundTrainListLoaded: true,
      isReturnTrainListLoaded: true,
      outboundTravelDateValidationError: false,
      outboundTravelDateValidationStatus: null,
      journeyTypeValidationError: false,
      journeyTypeValidationStatus: null,
      returnTravelDateValidationError: false,
      returnTravelDateValidationStatus: null,
      originStationValidationError: false,
      originStationValidationStatus: null,
      destinationStationValidationError: false,
      destinationStationValidationStatus: null,
      noOutboundTrainResults: false,
      isOutboundTrainErrored: false,
      noReturnTrainResults: false,
      isReturnTrainErrored: false,
      routeError: false,
      timeError: false,
      originStations: [],
      destinationStations: [],
      availableOutboundTrains: [],
      selectedOriginStation: null,
      selectedDestinationStation: null,
      selectedOriginStationText: null,
      selectedDestinationStationText: null,
      selectedOutboundTrain: null,
      selectedReturnTrain: null,
      outboundTrainValidationError: false,
      outboundTrainValidationStatus: null,
      returnTrainValidationError: false,
      returnTrainValidationStatus: null,
      outboundTravelDate: moment(),
      returnTravelDate: moment().add(1, 'days'),
      journeyType: '',
      returnJourney: false,
      showOutboundLoader: false,
      showReturnLoader: false,
      canSubmit: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = event.target.name;

    this.setState({
      [name]: value
    });

    if (name === "journeyType" && value === "Return") {
      this.setState({
        returnJourney: true
      });
    } else if (name === "journeyType" && value !== "Return") {
      this.setState({
        returnJourney: false
      });
    }
  }

  handleOriginStationChange = (selectedOption) => {
    this.setState({
      selectedOriginStation: selectedOption.value,
      selectedOriginStationText: selectedOption.label
    });

    this.createOutboundTrainList(this.state.outboundTravelDate, selectedOption.value, this.state.selectedDestinationStation);
    this.createReturnTrainList(this.state.returnTravelDate, selectedOption.value, this.state.selectedDestinationStation);
  }

  handleDestinationStationChange = (selectedOption) => {
    this.setState({
      selectedDestinationStation: selectedOption.value,
      selectedDestinationStationText: selectedOption.label
    });

    this.createOutboundTrainList(this.state.outboundTravelDate, this.state.selectedOriginStation, selectedOption.value);
    this.createReturnTrainList(this.state.returnTravelDate, this.state.selectedOriginStation, selectedOption.value);
  }

  handleOutboundDateChange = (date) => {
    this.setState({ outboundTravelDate: date });

    this.createOutboundTrainList(date, this.state.selectedOriginStation, this.state.selectedDestinationStation);
  }

  handleReturnDateChange = (date) => {
    this.setState({ returnTravelDate: date });

    this.createReturnTrainList(date, this.state.selectedOriginStation, this.state.selectedDestinationStation);
  }

  handleOutboundTrainSelection = (selectedOption) => {
    this.setState({ selectedOutboundTrain: selectedOption.value });

    if (selectedOption.value !== null && selectedOption.value !== undefined && selectedOption.value !== '' &&
       (!this.state.returnJourney || (this.state.returnJourney && this.state.selectedReturnTrain !== null &&
          this.state.selectedReturnTrain !== undefined && this.state.selectedReturnTrain !== ''))) {
      this.setState({ canSubmit: true });
    } else {
      this.setState({ canSubmit: false });
    }
  }

  handleReturnTrainSelection = (selectedOption) => {
    this.setState({ selectedReturnTrain: selectedOption.value });

    if (this.state.selectedOutboundTrain !== null && this.state.selectedOutboundTrain !== undefined && this.state.selectedOutboundTrain !== '' &&
       (!this.state.returnJourney || (this.state.returnJourney && selectedOption.value !== null &&
          selectedOption.value !== undefined && selectedOption.value !== ''))) {
      this.setState({ canSubmit: true });
    } else {
      this.setState({ canSubmit: false });
    }
  }

  createStationsLists() {
    this.setState({
      isOriginStationLoaded: false,
      isDestinationStationLoaded: false
    });

    fetch("https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/stations")
    .then(res => res.json())
    .then((json) => {
      initialStations = json.stations;
      var tempStations = [];

      for (var i in initialStations) {
        tempStations[i] = {"value": initialStations[i].crs, "label": initialStations[i].name};
      }

      this.setState({
        isOriginStationLoaded: true,
        isDestinationStationLoaded: true,
        originStations: tempStations,
        destinationStations: tempStations
      });
    },
    (error) => {
      this.setState({
        isOriginStationLoaded: true,
        isDestinationStationLoaded: true,
        isOriginStationErrored: true,
        isDestinationStationErrored: true,
        error
      });
    })
  }

  createOutboundTrainList(outboundTravelDate, selectedOriginStation, selectedDestinationStation) {
    this.setState({
      noOutboundTrainResults: false
    });

    if (outboundTravelDate !== null && outboundTravelDate !== undefined && outboundTravelDate !== "" &&
        selectedOriginStation !== null && selectedOriginStation !== undefined && selectedOriginStation !== "" &&
        selectedDestinationStation !== null && selectedDestinationStation !== undefined && selectedDestinationStation !== "") {
      this.setState({
        isOutboundTrainListLoaded: false,
        showOutboundLoader: true
      });

      fetch("https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/trains/" + selectedOriginStation + "/"
              + moment(outboundTravelDate).format("YYYY-MM-DD/HH:mm") + "?"
              + "destination=" + selectedDestinationStation)
      .then(res => res.json())
      .then((json) => {
        var tempTrains = [];

        if (json.departures.all !== null && json.departures.all !== undefined && json.departures.all !== "" && json.departures.all.length > 0) {
          initialTrains = json.departures.all;

          for (var i in initialTrains) {
            tempTrains[i] = {"value": moment(outboundTravelDate).format("DD/MM/YYYY") + " " + initialTrains[i].aimed_departure_time ,
                             "label": moment(outboundTravelDate).format("DD/MM/YYYY") + " " + initialTrains[i].aimed_departure_time + ". Operator: " + initialTrains[i].operator_name};
          }

          this.setState({
            isOutboundTrainListLoaded: true,
            showOutboundLoader: false,
            availableOutboundTrains: tempTrains
          });
        } else {
          this.setState({
            isOutboundTrainListLoaded: true,
            showOutboundLoader: false,
            noOutboundTrainResults: true
          });
        }
      },
      (error) => {
        this.setState({
          isOutboundTrainListLoaded: true,
          showOutboundLoader: false,
          isOutboundTrainErrored: true,
          error
        });
      })
    } else {
      this.setState({
        isOutboundTrainListLoaded: false,
        showOutboundLoader: false
      });
    }
  }

  async createReturnTrainList(returnTravelDate, selectedOriginStation, selectedDestinationStation) {
    this.setState({
      noResultsTrainResults: false
    });

    if (returnTravelDate !== null && returnTravelDate !== undefined && returnTravelDate !== "" &&
        selectedOriginStation !== null && selectedOriginStation !== undefined && selectedOriginStation !== "" &&
        selectedDestinationStation !== null && selectedDestinationStation !== undefined && selectedDestinationStation !== "") {
      this.setState({
        isReturnTrainListLoaded: false,
        showReturnLoader: true
      });

      await fetch("https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/trains/" + selectedDestinationStation + "/"
              + moment(returnTravelDate).format("YYYY-MM-DD/HH:mm") + "?"
              + "destination=" + selectedOriginStation)
      .then(res => res.json())
      .then((json) => {
        var tempTrains = [];

        if (json.departures.all !== null && json.departures.all !== undefined && json.departures.all !== "" && json.departures.all.length > 0) {
          initialTrains = json.departures.all;

          for (var i in initialTrains) {
            tempTrains[i] = {"value": moment(returnTravelDate).format("DD/MM/YYYY") + " " + initialTrains[i].aimed_departure_time ,
                             "label": moment(returnTravelDate).format("DD/MM/YYYY") + " " + initialTrains[i].aimed_departure_time + ". Operator: " + initialTrains[i].operator_name};
          }

          this.setState({
            isReturnTrainListLoaded: true,
            showReturnLoader: false,
            availableReturnTrains: tempTrains
          });
        } else {
          this.setState({
            isReturnTrainListLoaded: true,
            showReturnLoader: false,
            noReturnTrainResults: true
          });
        }
      },
      (error) => {
        this.setState({
          isReturnTrainListLoaded: true,
          showReturnLoader: false,
          isReturnTrainErrored: true,
          error
        });
      })
    } else {
      this.setState({
        isReturnTrainListLoaded: false,
        showReturnLoader: false
      });
    }
  }

  componentDidMount() {
    this.createStationsLists();
    this.createOutboundTrainList(null, null, null);
    this.createReturnTrainList(null, null, null);
  }

  handleClick(event) {
    var validated = this.validateForm(event);

    if (validated) {
      this.submitForm(event);
    }
  }

  validateForm(event) {
    var errorCnt = 0;

    this.setState({
      outboundTravelDateValidationError: false,
      returnTravelDateValidationError: false,
      journeyTypeValidationError: false,
      originStationValidationError: false,
      destinationStationValidationError: false,
      timeError: false
    });

    if (this.state.outboundTravelDate === null || this.state.outboundTravelDate === undefined || this.state.outboundTravelDate === "") {
      this.setState({
        outboundTravelDateValidationError: true,
        outboundTravelDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        outboundTravelDateValidationStatus: null
      });
    }

    if (this.state.journeyType === null || this.state.journeyType === undefined || this.state.journeyType === "") {
      this.setState({
        journeyTypeValidationError: true,
        journeyTypeValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        journeyTypeValidationStatus: null
      });
    }

    if ((this.state.returnTravelDate === null || this.state.returnTravelDate === undefined || this.state.returnTravelDate === "") &&
        this.state.returnJourney) {
      this.setState({
        returnTravelDateValidationError: true,
        returnTravelDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        returnTravelDateValidationStatus: null
      });
    }

    if (this.state.selectedOriginStation === null || this.state.selectedOriginStation === undefined || this.state.selectedOriginStation === "") {
      this.setState({
        originStationValidationError: true,
        originStationValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        originStationValidationStatus: null
      });
    }

    if (this.state.selectedDestinationStation === null || this.state.selectedDestinationStation === undefined || this.state.selectedDestinationStation === "") {
      this.setState({
        destinationStationValidationError: true,
        destinationStationValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        destinationStationValidationStatus: null
      });
    }

    if ((this.state.selectedOutboundTrain === null || this.state.selectedOutboundTrain === undefined || this.state.selectedOutboundTrain === '') &&
        !this.state.noOutboundTrainResults) {
      this.setState({
        outboundTrainValidationError: true,
        outboundTrainValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        outboundTrainValidationStatus: null
      });
    }

    if ((this.state.selectedReturnTrain === null || this.state.selectedReturnTrain === undefined || this.state.selectedReturnTrain === '') &&
         this.state.returnJourney && !this.state.noReturnTrainResults) {
      this.setState({
        returnTrainValidationError: true,
        returnTrainValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        returnTrainValidationStatus: null
      });
    }

    if (errorCnt > 0) {
      event.preventDefault();

      return false;
    } else {
      return true;
    }
  }

  async submitForm(event) {
    event.preventDefault();
    await fetch('https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/trains', {
      method: "POST",
      mode: "cors",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        employeeId: this.props.employeeId,
        travelType: 'train',
        journeyType: this.state.journeyType,
        outboundTrain: this.state.selectedOriginStationText + ": " + this.state.selectedOutboundTrain,
        returnTrain: this.state.returnJourney ? this.state.selectedDestinationStationText  + ": " + this.state.selectedReturnTrain : 'N/A'
      })
    }).then(function(response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(resp => {
        alert("Your train booking request has been submitted. You should hear from the booking company shortly. Thank you!");
        this.props.redirectToHome();
      })
      .catch(error => alert("An error occurred, please try again later. Error: " + error));
  }

  render() {
    const { selectedOption } = this.state;

    return (
      <div className="trainForm">
        <form>
          <FormGroup controlId="outboundTravelDate" id="outboundTravelDate" validationState={this.state.outboundTravelDateValidationStatus}>
            <ControlLabel>Leaving Date</ControlLabel>
            <DatePicker dateFormat="DD/MM/YYYY HH:mm"
                        placeholderText="Click to select a date and time"
                        minDate={moment()}
                        className="form-control"
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={15}
                        timeCaption="Time"
                        selected={this.state.outboundTravelDate}
                        onChange={this.handleOutboundDateChange} />
            {this.state.outboundTravelDateValidationError && <HelpBlock>Please select an outbound travel date</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup validationState={this.state.journeyTypeValidationStatus}>
            <ControlLabel>Journey Type</ControlLabel>
            <Radio name="journeyType" value="Single" onChange={this.handleInputChange}>
              Single
            </Radio>{'  '}
            <Radio name="journeyType" value="Return" onChange={this.handleInputChange}>
              Return
            </Radio>{'  '}
            <Radio name="journeyType" value="Open Return" onChange={this.handleInputChange}>
              Open Return
            </Radio>
            {this.state.journeyTypeValidationError && <HelpBlock>Please select a journey type</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          {this.state.returnJourney &&
            <FormGroup controlId="returnTravelDate" id="returnTravelDate" validationState={this.state.returnTravelDateValidationStatus}>
              <ControlLabel>Returning Date</ControlLabel>
              <DatePicker dateFormat="DD/MM/YYYY HH:mm"
                          placeholderText="Click to select a date and time"
                          minDate={moment()}
                          className="form-control"
                          showTimeSelect
                          timeFormat="HH:mm"
                          timeIntervals={15}
                          timeCaption="Time"
                          selected={this.state.returnTravelDate}
                          onChange={this.handleReturnDateChange} />
              {this.state.returnTravelDateValidationError && <HelpBlock>Please select a return travel date</HelpBlock>}
              <FormControl.Feedback />
            </FormGroup>
          }
          <FormGroup controlId="originStationList" id="originStationList" validationState={this.state.originStationValidationStatus}>
            <ControlLabel>Origin Station</ControlLabel>
            {this.state.isOriginStationLoaded && !this.state.isOriginStationErrored && <Select value={selectedOption} onChange={this.handleOriginStationChange} options={this.state.originStations} placeholder="Select the origin station" />}
            {!this.state.isOriginStationLoaded && <Loader type="TailSpin" color="light-grey" height={80} width={80} />}
            {this.state.originStationValidationError && <HelpBlock>Please select an origin station</HelpBlock>}
            {this.state.isOriginStationErrored && <OriginStationErrorAlert />}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup controlId="destinationStationList" id="destinationStationList" validationState={this.state.destinationStationValidationStatus}>
            <ControlLabel>Destination Station</ControlLabel>
            {this.state.isDestinationStationLoaded && !this.state.isDestinationStationErrored && <Select value={selectedOption} onChange={this.handleDestinationStationChange} options={this.state.destinationStations} placeholder="Select the destination station" />}
            {!this.state.isDestinationStationLoaded && <Loader type="TailSpin" color="light-grey" height={80} width={80} />}
            {this.state.destinationStationValidationError && <HelpBlock>Please select a destination station</HelpBlock>}
            {this.state.isDestinationStationErrored && <DestinationStationErrorAlert />}
            <FormControl.Feedback />
          </FormGroup>
          {this.state.isOutboundTrainListLoaded &&
            <FormGroup controlId="outboundTrainList" id="outboundTrainList" validationState={this.state.outboundTrainValidationStatus}>
              <ControlLabel>Outbound Train</ControlLabel>
              {!this.state.noOutboundTrainResults && <Select value={selectedOption} onChange={this.handleOutboundTrainSelection} options={this.state.availableOutboundTrains} placeholder="Select the outbound train" />}
              {this.state.outboundTrainValidationError && <HelpBlock>Please select an outbound train</HelpBlock>}
              {this.state.noOutboundTrainResults && <NoOutboundTrainAlert />}
              {this.state.isOutboundTrainErrored && <OutboundTrainErrorAlert />}
              <FormControl.Feedback />
            </FormGroup>
          }
          {!this.state.isOutboundTrainListLoaded && this.state.showOutboundLoader && <Loader type="TailSpin" color="light-grey" height={80} width={80} />}
          {this.state.returnJourney && this.state.isReturnTrainListLoaded &&
            <FormGroup controlId="returnTrainList" id="returnTrainList" validationState={this.state.returnTrainValidationStatus}>
              <ControlLabel>Return Train</ControlLabel>
              {!this.state.noReturnTrainResults && <Select value={selectedOption} onChange={this.handleReturnTrainSelection} options={this.state.availableReturnTrains} placeholder="Select the return train" />}
              {this.state.returnTrainValidationError && <HelpBlock>Please select a return train</HelpBlock>}
              {this.state.noReturnTrainResults && <NoReturnTrainAlert />}
              {this.state.isReturnTrainErrored && <ReturnTrainErrorAlert />}
              <FormControl.Feedback />
            </FormGroup>
          }
          {this.state.returnJourney && !this.state.isReturnTrainListLoaded && this.state.showReturnLoader && <Loader type="TailSpin" color="light-grey" height={80} width={80} />}
          <Button id="submitTrainBooking" bsStyle="primary" onClick={this.handleClick} disabled={!this.state.canSubmit}>Book Train</Button>
        </form>
      </div>
    );
  }
}

const NoOutboundTrainAlert = () => (
  <Alert bsStyle="warning">
    <b>There are no trains available for the outbound route on the chosen date/time</b>
    <br />Please choose another route or date/time
  </Alert>
)

const NoReturnTrainAlert = () => (
  <Alert bsStyle="warning">
    <b>There are no trains available for the return route on the chosen date/time</b>
    <br />Please choose another route or date/time
  </Alert>
)

const OriginStationErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving origin station details</b>
    <br />Please refresh the page and try again
  </Alert>
)

const DestinationStationErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving destination station details</b>
    <br />Please refresh the page and try again
  </Alert>
)

const OutboundTrainErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving outbound train details</b>
    <br />Please refresh the page and try again
  </Alert>
)

const ReturnTrainErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving return train details</b>
    <br />Please refresh the page and try again
  </Alert>
)

export { TrainForm };
