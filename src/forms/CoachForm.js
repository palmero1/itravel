import React, { Component } from 'react';
import { HelpBlock, FormGroup, FormControl, Radio, Button, ControlLabel, Alert } from 'react-bootstrap';
import Loader from 'react-loader-spinner';
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';
import './CoachForm.css';

var initialCoaches;

class CoachForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      travelDate: moment(),
      selectedCoach: '',
      coachResults: false,
      routeRadio: '',
      isCoachListErrored: false,
      noCoachesAvailable: false,
      error: null,
      isLoaded: true,
      routeValidationError: false,
      routeValidationStatus: null,
      travelDateValidationError: false,
      travelDateValidationStatus: null,
      coachValidationError: false,
      coachValidationStatus: null,
      coaches: [],
      canSubmit: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleCoachSelection = this.handleCoachSelection.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = event.target.name;

    this.setState({
      [name]: value
    });

    if (name === 'travelDate' && value !== null && value !== undefined && value !== "" &&
        this.state.routeRadio !== null && this.state.routeRadio !== undefined && this.state.routeRadio !== "" ) {
      this.createCoachList(this.state.routeRadio, value);
    } else if (name === 'routeRadio' && value !== null && value !== undefined && value !== "" &&
               this.state.travelDate !== null && this.state.travelDate !== undefined && this.state.travelDate !== "") {
      this.createCoachList(value, this.state.travelDate);
    }
  }

  handleCoachSelection = (selectedOption) => {
    this.setState({
      selectedCoach: selectedOption.value
    });

    if (selectedOption.value !== null && selectedOption.value !== undefined && selectedOption.value !== '') {
      this.setState({ canSubmit: true });
    } else {
      this.setState({ canSubmit: false });
    }
  }

  handleClick(event) {
    var validated = this.validateForm(event);

    if (validated) {
      this.submitForm(event);
    }
  }

  handleDateChange = (date) => {
    this.setState({ travelDate: date });

    if (date !== null && date !== undefined && date !== "" &&
        this.state.routeRadio !== null && this.state.routeRadio !== undefined && this.state.routeRadio !== "" ) {
      this.createCoachList(this.state.routeRadio, date);
    }
  }

  createCoachList(route, travelDate) {
    if (route !== null && travelDate !== null) {
      this.setState({isLoaded: false});

      fetch("https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/coaches?travel_date=" + moment(travelDate).format("YYYY-MM-DD") + "&from_destination=" + route)
      .then(res => res.json())
      .then((json) => {
        initialCoaches = json;

        if (json.Count > 0) {
          var tempCoaches = [];

          for (var i in initialCoaches.Items) {
            if (initialCoaches.Items[i].seats_available.N > 0) {
              tempCoaches[i] = {"value": initialCoaches.Items[i].time_leaving.S,
                                "label": initialCoaches.Items[i].time_leaving.S + ". Seats Available: " + initialCoaches.Items[i].seats_available.N};
            }
          }

          this.setState({
            isLoaded: true,
            coaches: tempCoaches,
            coachResults: true
          });

          this.setState({ isCoachListErrored: false });
        } else {
          this.setState({
            isLoaded: true,
            isCoachListErrored: true,
            coachResults: false
          });
        }
      },
      (error) => {
        this.setState({
          isLoaded: true,
          noCoachesAvailable: true,
          coachResults: false,
          error
        });
      })
    } else {
      this.setState({
        isLoaded: true
      });
    }
  }

  componentDidMount() {
    this.createCoachList(null, null);
  }

  validateForm(event) {
    var errorCnt = 0;

    this.setState({
      routeValidationError: false,
      routeValidationStatus: null,
      travelDateValidationError: false,
      dateValidationStatus: null,
      coachValidationError: false,
      timeValidationStatus: null
    });

    if (this.state.routeRadio === null || this.state.routeRadio === undefined || this.state.routeRadio === "") {
      this.setState({
        routeValidationError: true,
        routeValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        routeValidationStatus: null
      });
    }

    if (this.state.travelDate === null || this.state.travelDate === undefined || this.state.travelDate === "") {
      this.setState({
        travelDateValidationError: true,
        travelDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        travelDateValidationStatus: null
      });
    }

    if ((this.state.selectedCoach === null || this.state.selectedCoach === undefined || this.state.selectedCoach === "") &&
        (this.state.travelDate !== null || this.state.travelDate !== undefined || this.state.travelDate !== "") &&
        (this.state.routeRadio !== null || this.state.routeRadio !== undefined || this.state.routeRadio !== "")) {
      this.setState({
        coachValidationError: true,
        coachValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        coachValidationStatus: null
      });
    }

    if (errorCnt > 0) {
      event.preventDefault();

      return false;
    } else {
      return true;
    }
  }

  async submitForm(event) {
    event.preventDefault();
    await fetch('https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/coaches', {
      method: "POST",
      mode: "cors",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        employeeId: this.props.employeeId,
        travelType: 'coach',
        travelDate:  moment(this.state.travelDate).format("DD/MM/YYYY"),
        selectedCoach: this.state.routeRadio,
    		travelDetails: this.state.selectedCoach
      })
    }).then(function(response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(resp => {
        alert("Your coach booking request has been submitted. You should hear from the booking company shortly. Thank you!");
        this.props.redirectToHome();
      })
      .catch(error => alert("An error occurred, please try again later. Error: " + error));
  }

  render() {
    const { selectedOption } = this.state;

    return (
      <div className="coachForm">
        <form>
          <FormGroup validationState={this.state.travelDateValidationStatus}>
            <ControlLabel>Travel Date</ControlLabel>
            <DatePicker name="travelDate"
                        todayButton={"Today"}
                        dateFormat="DD/MM/YYYY"
                        placeholderText="Click to select a date"
                        minDate={moment()}
                        locale="en-gb"
                        className="form-control"
                        selected={this.state.travelDate}
                        onChange={this.handleDateChange} />
            {this.state.travelDateValidationError && <HelpBlock>Please select a date for travel</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup validationState={this.state.routeValidationStatus}>
            <ControlLabel>Journey Route</ControlLabel>
            <Radio name="routeRadio" value="Cheltenham" onChange={this.handleInputChange}>
              Cheltenham to London
            </Radio>{'  '}
            <Radio name="routeRadio" value="London" onChange={this.handleInputChange}>
              London to Cheltenham
            </Radio>
            {this.state.routeValidationError && <HelpBlock>Please select a route</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          {!this.state.isLoaded && <Loader type="TailSpin" color="light-grey" height={80} width={80} />}
          {this.state.coachResults && <FormGroup controlId="coach-list" id="coachList" validationState={this.state.coachValidationStatus}>
            <ControlLabel>Coach Time</ControlLabel>
            <Select value={selectedOption} onChange={this.handleCoachSelection} options={this.state.coaches} placeholder="Select an available coach" />
            {this.state.coachValidationError && <HelpBlock>Please select a coach time</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>}
          {this.state.noCoachesAvailable && <ErrorAlert />}
          {this.state.isCoachListErrored && <NoCoachAlert />}
          <Button id="submitCoachBooking" bsStyle="primary" onClick={this.handleClick} disabled={!this.state.canSubmit}>Book Coach</Button>
        </form>
      </div>
    );
  }
}

const NoCoachAlert = () => (
  <Alert bsStyle="warning">
    <b>There are no coaches available for this day</b>
    <p>Please choose another date or try again later as there may have been a cancellation</p>
  </Alert>
)

const ErrorAlert = () => (
  <Alert bsStyle="danger">
    <b>An error has occurred while retrieving coach times</b>
    <p>Please refresh the page and try again</p>
  </Alert>
)

export { CoachForm };
