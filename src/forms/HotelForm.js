import React, { Component } from 'react';
import { HelpBlock, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import PlacesAutocomplete, { geocodeByAddress, getLatLng} from 'react-places-autocomplete';

import 'react-datepicker/dist/react-datepicker.css';
import './HotelForm.css';

class HotelForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: '',
      checkInDate: moment(),
      checkOutDate: moment(),
      latitude: null,
      longitude: null,
      error: null,
      addressValidationError: false,
      addressValidationStatus: null,
      checkInDateValidationError: false,
      checkInDateValidationStatus: null,
      checkOutDateValidationError: false,
      checkOutDateValidationStatus: null,
      checkInDateGreaterThanTripCheckOutError: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = event.target.name;

    this.setState({
      [name]: value
    });
  }

  handlePlaceChange = address => {
    this.setState({
      address,
      latitude: null,
      longitude: null
    });

    if (address === null || address === undefined || address === '') {
      this.setState({ canSubmit: false });
    }
  };

  handlePlaceSelect = selected => {
    this.setState({ address: selected });

    if (selected === null || selected === undefined || selected === '') {
      this.setState({ canSubmit: false });
    }

    geocodeByAddress(selected)
      .then(results => getLatLng(results[0]))
      .then(({ lat, lng }) => {
        this.setState({
          latitude: lat,
          longitude: lng,
          canSubmit: true
        });
      })
      .catch(error => console.error('Error', error));
  };

  handleClick(event) {
    var validated = this.validateForm(event);

    if (validated) {
      this.submitForm(event);
    }
  }

  handleCheckInDateChange = (date) => {
    this.setState({ checkInDate: date });
  }

  handleCheckOutDateChange = (date) => {
    this.setState({ checkOutDate: date });
  }

  validateForm(event) {
    var errorCnt = 0;

    this.setState({
      addressValidationError: false,
      checkInDateValidationError: false,
      checkOutDateValidationError: false,
      checkInDateGreaterThanTripCheckOutError: false
    });

    if (this.state.address === null || this.state.address === undefined || this.state.address === "") {
      this.setState({
        addressValidationError: true,
        addressValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        addressValidationError: false,
        addressValidationStatus: null
      });
    }

    if (this.state.checkInDate === null || this.state.checkInDate === undefined || this.state.checkInDate === "") {
      this.setState({
        checkInDateValidationError: true,
        checkInDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        checkInDateValidationError: false,
        checkInDateValidationStatus: null
      });
    }

    if (this.state.checkOutDate === null || this.state.checkOutDate === undefined || this.state.checkOutDate === "") {
      this.setState({
        checkOutDateValidationError: true,
        checkOutDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        checkOutDateValidationError: false,
        checkOutDateValidationStatus: null
      });
    }

    if ((this.state.checkInDate !== null || this.state.checkInDate !== undefined || this.state.checkInDate !== "") &&
       (this.state.checkOutDate !== null || this.state.checkOutDate !== undefined || this.state.checkOutDate !== "") &&
       (this.state.checkInDate > this.state.tripEndDate)) {
      this.setState({
        checkInDateGreaterThanTripCheckOutError: true,
        checkInDateValidationStatus: 'error',
        checkOutDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    }

    if (errorCnt > 0) {
      event.preventDefault();

      return false;
    } else {
      return true;
    }
  }

  async submitForm(event) {
    event.preventDefault();
    await fetch('https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/hotels', {
      method: "POST",
      mode: "cors",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        employeeId: this.props.employeeId,
        bookingType: 'hotel',
        location: this.state.address,
        checkInDate:  moment(this.state.checkInDate).format("DD/MM/YYYY"),
        checkOutDate:  moment(this.state.checkOutDate).format("DD/MM/YYYY"),
    		latLng: this.state.latitude + "," + this.state.longitude
      })
    }).then(function(response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(resp => {
        alert("Your hotel booking request has been submitted. You should hear from the booking company shortly. Thank you!");
        this.props.redirectToHome();
      })
      .catch(error => alert("An error occurred, please try again later. Error: " + error));
    }

  render() {
    return (
      <div className="hotelForm">
        <form>
          <FormGroup validationState={this.state.addressValidationStatus}>
            <ControlLabel>Hotel Location</ControlLabel>
            <PlacesAutocomplete value={this.state.address} onChange={this.handlePlaceChange} onSelect={this.handlePlaceSelect}>
              {renderAutocomplete}
            </PlacesAutocomplete>
            {this.state.addressValidationError && <HelpBlock>Please select a location</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup validationState={this.state.checkInDateValidationStatus}>
            <ControlLabel>Check in</ControlLabel>
            <DatePicker name="checkInDate"
                        todayButton={"Today"}
                        dateFormat="DD/MM/YYYY"
                        placeholderText="Click to select a check in date"
                        minDate={moment()}
                        locale="en-gb"
                        className="form-control"
                        selected={this.state.checkInDate}
                        onChange={this.handleCheckInDateChange} />
            {this.state.checkInDateValidationError && <HelpBlock>Please select a check in date</HelpBlock>}
            {this.state.checkInDateGreaterThanTripCheckOutError && <HelpBlock>Check In Date must be before Check Out Date</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup validationState={this.state.checkOutDateValidationStatus}>
            <ControlLabel>Check out</ControlLabel>
            <DatePicker name="checkOutDate"
                        todayButton={"Today"}
                        dateFormat="DD/MM/YYYY"
                        placeholderText="Click to select a check out date"
                        minDate={moment()}
                        locale="en-gb"
                        className="form-control"
                        selected={this.state.checkOutDate}
                        onChange={this.handleCheckOutDateChange} />
            {this.state.checkOutDateValidationError && <HelpBlock>Please select a check out date</HelpBlock>}
            {this.state.checkInDateGreaterThanTripCheckOutError && <HelpBlock>Check In Date must be before Check Out Date</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <Button id="submitHotelBooking" bsStyle="primary" onClick={this.handleClick} disabled={!this.state.canSubmit}>Book Hotel</Button>
        </form>
      </div>
    );
  }
}

const renderAutocomplete = ({ getInputProps, suggestions, getSuggestionItemProps }) => (
  <div>
    <input
      {...getInputProps({
        placeholder: 'Search Location ...',
        className: 'form-control',
      })}
    />
    <div className="autocomplete-places-dropdown-container">
      {suggestions.map(suggestion => {
        const className = suggestion.active
          ? 'suggestion-item--active'
          : 'suggestion-item';
        // inline style for demonstration purpose
        const style = suggestion.active
          ? { backgroundColor: '#c2e0ff', cursor: 'pointer' }
          : { backgroundColor: '#ffffff', cursor: 'pointer' };
        return (
          <div
            {...getSuggestionItemProps(suggestion, {
              className,
              style,
            })}
          >
            <span>{suggestion.description}</span>
          </div>
        );
      })}
    </div>
  </div>
)

export { HotelForm };
