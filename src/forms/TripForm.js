import React, { Component } from 'react';
import { HelpBlock, FormGroup, FormControl, Button, ControlLabel } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import PlacesAutocomplete, { geocodeByAddress, getLatLng} from 'react-places-autocomplete';

import 'react-datepicker/dist/react-datepicker.css';
import './TripForm.css';

class TripForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tripStartDate: moment(),
      tripEndDate: moment(),
      coachId: '',
      routeRadio: '',
      alertIsHidden: true,
      isErrored: false,
      error: null,
      isLoaded: true,
      addressValidationError: false,
      addressValidationStatus: null,
      tripStartDateValidationError: false,
      tripStartDateValidationStatus: null,
      tripEndDateValidationError: false,
      tripEndDateValidationStatus: null,
      tripStartDateGreaterThanTripEndDateError: false,
      routeError: false,
      dateError: false,
      timeError: false,
      coaches: [],
      address: '',
      latitude: null,
      longitude: null
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = event.target.name;

    this.setState({
      [name]: value
    });
  }

  handlePlaceChange = address => {
    this.setState({
      address,
      latitude: null,
      longitude: null
    });

    if (address === null || address === undefined || address === '') {
      this.setState({ canSubmit: false });
    }
  };

  handlePlaceSelect = selected => {
    this.setState({ address: selected });

    if (selected === null || selected === undefined || selected === '') {
      this.setState({ canSubmit: false });
    }

    geocodeByAddress(selected)
      .then(results => getLatLng(results[0]))
      .then(({ lat, lng }) => {
        this.setState({
          latitude: lat,
          longitude: lng,
          canSubmit: true
        });
      })
      .catch(error => console.error('Error', error));
  };

  handleClick(event) {
    var validated = this.validateForm(event);

    if (validated) {
      this.submitForm(event);
    }
  }

  handleTripStartDateChange = (date) => {
    this.setState({ tripStartDate: date });
  }

  handleTripEndDateChange = (date) => {
    this.setState({ tripEndDate: date });
  }

  validateForm(event) {
    var errorCnt = 0;

    this.setState({
      addressValidationError: false,
      tripStartDateValidationError: false,
      tripEndDateValidationError: false,
      tripStartDateGreaterThanTripEndDateError: false
    });

    if (this.state.address === null || this.state.address === undefined || this.state.address === "") {
      this.setState({
        addressValidationError: true,
        addressValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        addressValidationError: false,
        addressValidationStatus: null
      });
    }

    if (this.state.tripStartDate === null || this.state.tripStartDate === undefined || this.state.tripStartDate === "") {
      this.setState({
        tripStartDateValidationError: true,
        tripStartDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        tripStartDateValidationError: false,
        tripStartDateValidationStatus: null
      });
    }

    if (this.state.tripEndDate === null || this.state.tripEndDate === undefined || this.state.tripEndDate === "") {
      this.setState({
        tripEndDateValidationError: true,
        tripEndDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    } else {
      this.setState({
        tripEndDateValidationError: false,
        tripEndDateValidationStatus: null
      });
    }

    if ((this.state.tripStartDate !== null || this.state.tripStartDate !== undefined || this.state.tripStartDate !== "") &&
       (this.state.tripEndDate !== null || this.state.tripEndDate !== undefined || this.state.tripEndDate !== "") &&
       (this.state.tripStartDate > this.state.tripEndDate)) {
      this.setState({
        tripStartDateGreaterThanTripEndDateError: true,
        tripStartDateValidationStatus: 'error',
        tripEndDateValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    }

    if (errorCnt > 0) {
      event.preventDefault();

      return false;
    } else {
      return true;
    }
  }

 async submitForm(event) {
    event.preventDefault();
    await fetch('https://tddq5z7kl9.execute-api.eu-west-2.amazonaws.com/DEV/trips', {
      method: "POST",
      mode: "cors",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        employeeId: this.props.employeeId,
        tripLocation: this.state.address,
        tripStartDate:  moment(this.state.tripStartDate).format("DD/MM/YYYY"),
        tripEndDate:  moment(this.state.tripEndDate).format("DD/MM/YYYY"),
    		latLng: this.state.latitude + "," + this.state.longitude
      })
    }).then(function(response) {
      if (!response.ok) {
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(resp => {
      alert("Your trip has been submitted. Thank you!");
      this.props.redirectToHome();
    })
    .catch(error => alert("An error occurred, please try again later. Error: " + error));
  }

  render() {
    return (
      <div className="tripForm">
        <form>
          <FormGroup validationState={this.state.addressValidationStatus}>
            <ControlLabel>Trip Location</ControlLabel>
            <PlacesAutocomplete value={this.state.address} onChange={this.handlePlaceChange} onSelect={this.handlePlaceSelect}>
              {renderAutocomplete}
            </PlacesAutocomplete>
            {this.state.addressValidationError && <HelpBlock>Please enter a location</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup validationState={this.state.tripStartDateValidationStatus}>
            <ControlLabel>Trip Start Date</ControlLabel>
            <DatePicker name="tripStartDate"
                        todayButton={"Today"}
                        dateFormat="DD/MM/YYYY"
                        placeholderText="Click to select a trip start date"
                        locale="en-gb"
                        className="form-control"
                        selected={this.state.tripStartDate}
                        onChange={this.handleTripStartDateChange} />
            {this.state.tripStartDateValidationError && <HelpBlock>Please select a trip start date</HelpBlock>}
            {this.state.tripStartDateGreaterThanTripEndDateError && <HelpBlock>Start Date must be before End Date</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup validationState={this.state.tripEndDateValidationStatus}>
            <ControlLabel>Trip End Date</ControlLabel>
            <DatePicker name="tripEndDate"
                        todayButton={"Today"}
                        dateFormat="DD/MM/YYYY"
                        placeholderText="Click to select a trip end date"
                        locale="en-gb"
                        className="form-control"
                        selected={this.state.tripEndDate}
                        onChange={this.handleTripEndDateChange} />
            {this.state.tripEndDateValidationError && <HelpBlock>Please select a trip end date</HelpBlock>}
            {this.state.tripStartDateGreaterThanTripEndDateError && <HelpBlock>Start Date must be before End Date</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <Button id="submitTrip" bsStyle="primary" onClick={this.handleClick} disabled={!this.state.canSubmit}>Record Trip</Button>
        </form>
      </div>
    );
  }
}

const renderAutocomplete = ({ getInputProps, suggestions, getSuggestionItemProps }) => (
  <div>
    <input
      {...getInputProps({
        placeholder: 'Search Location ...',
        className: 'form-control',
      })}
    />
    <div className="autocomplete-places-dropdown-container">
      {suggestions.map(suggestion => {
        const className = suggestion.active
          ? 'suggestion-item--active'
          : 'suggestion-item';
        // inline style for demonstration purpose
        const style = suggestion.active
          ? { backgroundColor: '#c2e0ff', cursor: 'pointer' }
          : { backgroundColor: '#ffffff', cursor: 'pointer' };
        return (
          <div
            {...getSuggestionItemProps(suggestion, {
              className,
              style,
            })}
          >
            <span>{suggestion.description}</span>
          </div>
        );
      })}
    </div>
  </div>
)

export { TripForm };
