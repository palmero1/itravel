import React, { Component } from "react";
import { PageHeader } from 'react-bootstrap';
import "./HotelBooking.css";
import { HotelForm } from '../forms/HotelForm.js'

export default class HotelBooking extends Component {
  constructor(props){
    super(props);

    this.redirectToHome = this.redirectToHome.bind(this)
  }

  redirectToHome() {
    this.props.history.push("/");
  }

  render() {
    return (
      <div className="HotelBooking container form">
        <div className="lander">
          <PageHeader>
            Hotel
          </PageHeader>
          <HotelForm employeeId={this.props.employeeId} isAuthenticated={this.props.isAuthenticated} redirectToHome={this.redirectToHome}/>
        </div>
      </div>
    );
  }
}
