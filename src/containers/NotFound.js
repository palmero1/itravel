import React, { Component } from "react";
import "./NotFound.css";

export default class Login extends Component {
  render() {
    return (
      <div className="NotFound">
        <h1>Page Not Found</h1>
        <p>Please try another</p>
      </div>
    );
  }
}
