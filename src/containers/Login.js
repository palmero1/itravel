import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel, HelpBlock } from "react-bootstrap";
import "./Login.css";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      usernameValidationError: false,
      usernameValidationStatus: null,
      password: "",
      passwordValidationError: false,
      passwordValidationStatus: null
    };
  }

  validateForm(event) {
    var errorCnt = 0;

    this.setState({
      usernameValidationError: false,
      passwordValidationError: false,
    });

    //there will be better authentication when the integration with Office365 happens
    if (this.state.username !== 'user1234') {
      this.setState({
        usernameValidationError: true,
        usernameValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    }

    //there will be better authentication when the integration with Office365 happens
    if (this.state.password !== 'password') {
      this.setState({
        passwordValidationError: true,
        passwordValidationStatus: 'error'
      });

      errorCnt = errorCnt + 1;
    }

    if (errorCnt > 0) {
      event.preventDefault();
      return false;
    } else {
      return true;
    }
  }

  disableButton() {
    return this.state.username.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    var validated = this.validateForm(event);

    if (this.state.username === 'user1234' && this.state.password === 'password' && validated) {
      this.props.userHasAuthenticated(true, this.state.username);
    }
  }

  render() {
    return (
      <div className="Login container form">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username" bsSize="large" validationState={this.state.usernameValidationStatus}>
            <ControlLabel>Username</ControlLabel>
            <FormControl
              autoFocus
              type="text"
              value={this.state.username}
              onChange={this.handleChange}
            />
            {this.state.usernameValidationError && <HelpBlock>Please enter an existing username</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large" validationState={this.state.usernameValidationStatus}>
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
            {this.state.passwordValidationError && <HelpBlock>Please enter an existing password</HelpBlock>}
            <FormControl.Feedback />
          </FormGroup>
          <Button
            block
            bsSize="large"
            type="submit"
            disabled={!this.disableButton()}
          >
            Login
          </Button>
        </form>
      </div>
    );
  }
}
