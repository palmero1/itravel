import React, { Component } from "react";
import { PageHeader } from 'react-bootstrap';
import "./RecordTravel.css";
import { TripForm } from '../forms/TripForm.js'

export default class RecordTravel extends Component {
  constructor(props){
    super(props);

    this.redirectToHome = this.redirectToHome.bind(this)
  }

  redirectToHome() {
    this.props.history.push("/");
  }

  render() {
    return (
      <div className="RecordTravel container form">
        <div className="lander">
          <PageHeader>
            Trip
          </PageHeader>
          <TripForm employeeId={this.props.employeeId} isAuthenticated={this.props.isAuthenticated} redirectToHome={this.redirectToHome}/>
        </div>
      </div>
    );
  }
}
