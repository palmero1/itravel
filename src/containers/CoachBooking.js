import React, { Component } from "react";
import { PageHeader } from 'react-bootstrap';
import "./CoachBooking.css";
import { CoachForm } from '../forms/CoachForm.js'

export default class CoachBooking extends Component {
  constructor(props){
    super(props);

    this.redirectToHome = this.redirectToHome.bind(this)
  }

  redirectToHome() {
    this.props.history.push("/");
  }

  render() {
    return (
      <div className="CoachBooking container form">
        <div className="lander">
          <PageHeader>
            Coach
          </PageHeader>
        </div>
        <CoachForm employeeId={this.props.employeeId} isAuthenticated={this.props.isAuthenticated} redirectToHome={this.redirectToHome}/>
      </div>
    );
  }
}
