import React, { Component } from "react";
import { PageHeader } from 'react-bootstrap';

import BookingsTable from "../components/BookingsTable.js";
import TripsTable from "../components/TripsTable.js";

import 'react-table/react-table.css'
import "./Home.css";

export default class Home extends Component {
  render() {
    return (
      <div className="Home container form">
        <div className="lander">
          <PageHeader>
            Home - Welcome {this.props.employeeId}
          </PageHeader>
        </div>
        <BookingsTable employeeId={this.props.employeeId} />
        <TripsTable employeeId={this.props.employeeId} />
      </div>
    );
  }
}
