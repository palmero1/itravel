import React, { Component } from "react";
import { PageHeader } from 'react-bootstrap';
import "./TrainBooking.css";
import { TrainForm } from '../forms/TrainForm.js'

export default class TrainBooking extends Component {
  constructor(props){
    super(props);

    this.redirectToHome = this.redirectToHome.bind(this)
  }

  redirectToHome() {
    this.props.history.push("/");
  }

  render() {
    return (
      <div className="TrainBooking container form">
        <div className="lander">
          <PageHeader>
            Train
          </PageHeader>
          <TrainForm employeeId={this.props.employeeId} isAuthenticated={this.props.isAuthenticated} redirectToHome={this.redirectToHome}/>
        </div>
      </div>
    );
  }
}
