This is the iTravel application.

The application is designed in an API-first approach, which reside on AWS.

The object of the application is to allow the user to perform actions relating to travel. These are:
<ul>
  <li>Coach Booking
  <li>Train Booking
  <li>Hotel Booking
  <li>Record Trips
</ul>

This MUST be built and run on a Linux machine, based on Ubuntu.

To build and run the application, follow the instructions below.

To manually build and run the application, perform the following steps:

<ul>
  <li>sudo apt-get update
  <li>sudo apt-get upgrade
  <li>curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  <li>sudo apt-get install -y nodejs
  <li>npm Install
  <li>npm start
</ul>


To automatically build and run the application:

Run the install_script.sh script.
